﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/waterV"
{
    Properties {
        _Height ("Height", float) = 0
        _WaveAmp ("Wave Amplitude", float) = .1
        _WaveLength ("Wave Length", float) = 10
        _WavePhase ("Wave Phase", float) = 0
        _Center ("Center", Vector) = (0, 0, 0, 0)
        _Ratios ("Ratios", Vector) = (1, .8, .66, .5)
        _Weights ("Weights", Vector) = (1, .5, .25, .125)
        _WaveK ("K", float) = 2
        _PlayerPos ("Player Transform Pos", Vector) = (0, 1, 0)

        _Cube("Reflection Map", CUBE) = "" {}
    }
    SubShader {
      Tags { "Queue" = "Transparent" } 
         // draw after all opaque geometry has been drawn
      Pass {
         ZWrite Off // don't write to depth buffer 
            // in order not to occlude other objects

         Blend SrcAlpha OneMinusSrcAlpha // use alpha blending

            CGPROGRAM

            #pragma target 3.0

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            float _Height;
            float _WaveAmp;
            float _WaveLength;
            float _WavePhase;
            float2 _Center;
            float4 _Ratios;
            float4 _Weights;
            float _WaveK;
            float3 _PlayerPos;
            samplerCUBE _Cube;


            struct v2f {
                float4 pos : SV_POSITION;
                float3 norm: TEXCOORD1;
                float3 modelPos: TEXCOORD0;
            };

            v2f vert (appdata_base v)
            {
                v2f o;
                float4 modelPos = mul (unity_ObjectToWorld, v.vertex);
                float  surfMax = -1 * (modelPos.y - _Height);
                float  surfAmp = surfMax * _WaveAmp;

                float2 toCenter = modelPos.xz - _Center;
                float4 waveLengths = _WaveLength * _Ratios;
                float4 surfPhase = (length(toCenter) + _WavePhase)/ waveLengths;

                float4 surfWav = cos(surfPhase) * .5 + .5;
                float4 surfWavK = pow (surfWav, _WaveK); 
                float  surfH = surfAmp * (dot(surfWavK, normalize(_Weights)));

                float4 surfDerK = _WaveK * surfAmp * pow(surfWav, _WaveK - 1) 
                    * -1 * sin (surfPhase);
                float  surfDerAmp = dot(surfDerK, normalize(_Weights));
                float3 surfB = float3(0, (surfDerAmp * toCenter.y 
                                             / length(toCenter + .01)), 1);
                float3 surfT = float3(1, (surfDerAmp * toCenter.x
                                             / length(toCenter + .01)), 0);

                modelPos.y = _Height + surfH;

                o.pos = mul (UNITY_MATRIX_VP, modelPos);
                o.norm = normalize(cross(surfB, surfT));
                o.modelPos = modelPos.xyz / modelPos.w;

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
            	i.norm = normalize(i.norm);
                float3 dirToP = normalize(i.modelPos - _PlayerPos);
                float3 refDir = dirToP - 2 * dot (dirToP, i.norm) * i.norm;
            	float4 val = UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, refDir);
            	fixed4 refLight = .7;
                refLight.xyz = DecodeHDR(val, unity_SpecCube0_HDR);

                float diff = dot(i.norm, float3(1, 1, 1) / sqrt(3));
                diff = (diff + 1) / 2;
                //return fixed4 (i.norm.x, i.norm.y, i.norm.z, .5);
                fixed4 blueLight = fixed4 (0, diff * .3, diff, .7);

                return .2 * blueLight + .8 * refLight;
            }
            ENDCG

        }
    }
}
