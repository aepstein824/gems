﻿#pragma strict

var center:Vector2;
var reflectPlayer:GameObject;

function Start () {
    GetComponent.<Renderer>().material.SetVector ("_Center", center);
}

function Update () {
	GetComponent.<Renderer>().material.SetFloat ("_WavePhase", 
	                                             Time.realtimeSinceStartup *-2);
	GetComponent.<Renderer>().material.SetVector ("_PlayerPos",
	                                              reflectPlayer.transform.position);
}

