﻿Shader "Custom/cow" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Perlin Noise", 3D) = "cube" {}
		_Side ("Side", int) = 1
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_kSpot ("k Spot", float) = 0
		_iters ("num iterations", int) = 3
		_kRat ("iteration ratio", Range(1.1, 3)) = 2
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		#include  "../perlinnoise.cginc"


		sampler3D _MainTex;

		struct Input {
			float3 worldPos;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		int _Side;
		float _kSpot;
		int _iters;
		float _kRat;

		float bwnoise (float x) {
			return x < 0 ? 0 : 1;
		}


		void surf (Input IN, inout SurfaceOutputStandard o) {
			float3 pos = IN.worldPos;
			noiseGen gen = { _MainTex, _Side };
			float noiseSum = bwnoise(expSumNoise (gen, pos / 4, _kRat, _iters) + _kSpot); 

			fixed4 c = noiseSum * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
