﻿#pragma strict

public var side:int = 16;
public var player:GameObject;

@script ExecuteInEditMode()
function Start () {
  var cornerV = [[ 0,  1,  1],
                 [ 1,  0,  1],
                 [ 1,  1,  0],
                 [ 0,  1, -1],
                 [ 1,  0, -1],
                 [ 1, -1,  0],
                 [ 0, -1,  1],
                 [-1,  0,  1],
                 [-1,  1,  0],
                 [ 0, -1, -1],
                 [-1,  0, -1],
                 [-1, -1,  0]];

  var corners:Color[] = new Color [side * side * side];

  for (var i:int = 0; i < side; i++) 
  {
    for (var j:int = 0; j < side; j++) 
  {
      for (var k:int = 0; k < side; k++) 
  {
        var whichCorner:int = Mathf.Floor(UnityEngine.Random.Range(0, 12));
        var baseInd:int = k + side * j + side * side * i;
        corners[baseInd].r = cornerV[whichCorner][0] * .5 + .5;
        corners[baseInd].g = cornerV[whichCorner][1] * .5 + .5;
        corners[baseInd].b = cornerV[whichCorner][2] * .5 + .5;
        corners[baseInd].a = 1;
      }
    }
  }


  var tex3D:Texture3D = new Texture3D (side, side, side, TextureFormat.RGBA32, false);
  tex3D.SetPixels (corners);
  tex3D.filterMode = FilterMode.Point;
  tex3D.Apply ();

  for (i = 0; i < GetComponent.<Renderer>().sharedMaterials.length; i++){
        GetComponent.<Renderer>().sharedMaterials[i].SetTexture ("_PerlinVerts", 
	                                           tex3D);
        GetComponent.<Renderer>().sharedMaterials[i].SetInt ("_Side", side);
  }

  }

function Update () {
    for (var i:int = 0; i < GetComponent.<Renderer>().sharedMaterials.length; i++){
    
        GetComponent.<Renderer>().sharedMaterials[i].SetVector ("_PlayerPos", player.transform.position);
  }
  }