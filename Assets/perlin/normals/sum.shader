﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/sum" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)

		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Size ("Size", Range(0, 10)) = 1.0
		_Side ("Side", int) = 1
		_eps ("Normal Epsilon", Range(.00001,.1)) = .01
		_kEps ("Normal Scaling", Range(.001, 1)) = .03

		_MainTex ("Perlin Noise", 3D) = "" {}
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		#include  "../perlinnoise.cginc"

		sampler3D _MainTex;

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _Size;
		int _Side;
		float _eps;
		float _kEps;

		struct Input {
		    float3 worldPos;
			float3 sWorldNormal;
			float4 sWorldTangent;
		};

        void vert(inout appdata_full v, out Input o)
        {
        	UNITY_INITIALIZE_OUTPUT(Input, o);
            //v.normal = (float4( v.normal.x, v.normal.y, v.normal.z, 1.0)).xyz;
 
            o.sWorldNormal = mul((float3x3)unity_ObjectToWorld, SCALED_NORMAL);
            o.sWorldTangent = mul(v.tangent, unity_ObjectToWorld);
        }

		float bwnoise (float x) {
			return x < 0 ? 0 : 1;
		}

		float noiseFunc (float3 pos) {
			int iters = 2;
			noiseGen gen = { _MainTex, _Side };
			return -.1 * turbulence (gen, pos);
		}


		void surf (Input IN, inout SurfaceOutputStandard o) {
			float3 tangent = normalize(IN.sWorldTangent.xyz);
            float3 normal = normalize(IN.sWorldNormal);
            float3 binormal = normalize(cross(normal, tangent) * IN.sWorldTangent.w);
            float3x3 tangentToWorld = transpose(float3x3(tangent, binormal, normal));
            float3x3 worldToTangent = inverse(tangentToWorld);

			float3 pos = IN.worldPos/_Size;

		    fixed noiseSum   =        noiseFunc(pos);
		    float3 noiseGrad = float3(noiseFunc(pos + float3(_eps, 0, 0)),
		                              noiseFunc(pos + float3(0, _eps, 0)),
		                              noiseFunc(pos + float3(0, 0, _eps)));          
		    float3 dGrad = (noiseGrad - noiseSum * float3(1, 1, 1)) / _eps;

            float3 tanDGrad = mul (worldToTangent, dGrad);
            float3 tanNormal = normalize (float3(0, 0, 1) - _kEps * tanDGrad);



		    fixed4 c = noiseSum * _Color;
			o.Albedo = c.rgb;
			o.Normal = tanNormal;

			o.Albedo = _Color.rgb;


			o.Alpha = c.a;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
		}

		ENDCG
	}
	FallBack "Diffuse"
}
