﻿#pragma strict

public var side:int = 100;

function Start () {
	var mf:MeshFilter = GetComponent.<MeshFilter>();
	var mesh:Mesh = new Mesh();
	var inc:float = 1.0f / side;
	var verts: Vector3[] = new Vector3[(side + 1)*(side + 1)];
	var tris:int[] = new int[side * side * 6];
	for (var i:int = 0; i < side + 1; i++) {
		for (var j:int = 0; j < side + 1; j++) {
			var v:Vector3 = new Vector3(i * inc - .5, 0, 
										j * inc - .5	);
			var bl:int = i*(side+1) + j;
			verts[bl] = v;
			if (i != side && j != side) {
				var br:int = bl + 1;
				var tl:int = bl + side + 1;
				var tr:int = tl + 1;
				var tri:int = (i * side + j)  * 6;
				tris[tri    ] = tr;
				tris[tri + 1] = tl;
				tris[tri + 2] = br;
				tris[tri + 3] = bl;
				tris[tri + 4] = br;
				tris[tri + 5] = tl;
			}
		}
	}
	mesh.vertices = verts;
	mesh.triangles = tris;
	mf.mesh = mesh;

}

function Update () {

}