﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/mapsurf"
{
	Properties
	{
		_Height ("Height", float) = .1
		_ElevationTex ("Elevation Texture", 2D) = "" {}
		_Freq ("Frequency", float) = .125
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#define VERTEX_NOISE
			#include  "../perlinnoise.cginc"
			#undef VERTEX_NOISE

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				float  altitude : TEXCOORD0;
			};

			float4 _MainTex_ST;
			float _Height;
			sampler2D _ElevationTex;
			float _Freq;

			v2f vert (appdata v)
			{
				v2f o;
				noiseGenV gen = {16};
				float noiseVal = halfSumNoiseV(gen, float3(v.vertex.xz*_Freq, 0), 6);
				//float noiseVal = noiseV(gen, v.vertex.xyz * _Freq);
				v.vertex.y += _Height * noiseVal;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.altitude = noiseVal;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_ElevationTex, float2(i.altitude*.5 + .5, .5));
				return col;
			}
			ENDCG
		}
	}
}
