#ifndef VORONOI_H
#define VORONOI_H
		
#include  "../hashnoise.cginc"  

struct VoronoiResult {
	float f1, f2, fBorder;
	float2 world1, world2;
	float2 norm;
};

float2 iter2cand(float2 worldBL, float2 scanX, float2 iter2, float2 driftScale, bool skew) {
	float2 cell = worldBL + iter2;
	// cell starts at low,low corner of cell, so drift is 0 to 1
	float2 drift = driftScale * (hash22(cell) - (skew ? 0.5f: 0));

	if (skew && (2 * frac(cell.y / 2) > 0.5f)) iter2.x += 0.5f;
	float2 cand = iter2 + drift - scanX;

	return cand;
}

//our voronoi uses pos=0, but we want to be general
float3 barycentric(float2 pos, float2 A, float2 B, float2 C)
{
	// compute vectors        
	float2 v0 = A - C;
	float2 v1 = B - C;
	float2 v2 = pos - C;

	// compute dot products
	float dot00 = dot(v0, v0);
	float dot01 = dot(v0, v1);
	float dot02 = dot(v0, v2);
	float dot11 = dot(v1, v1);
	float dot12 = dot(v1, v2);

	// Compute barycentric coordinates
	float invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
	float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
	float v = (dot00 * dot12 - dot01 * dot02) * invDenom;
	return float3(u, v, 1 - v - u);
}

bool baryInTriangle(float3 bary) {
	return (bary.x >= 0 && bary.y >= 0 && bary.z >= 0);
}

float2 circumcenter(float2 A, float2 B, float2 C) {
	float2 center;
	center.yx = dot(A, A)*(B - C) + dot(B, B)*(C - A) + dot(C, C)*(A - B);
	center.y *= -1;
	center /= 2 * (A.x * (B.y - C.y) + B.x * (C.y - A.y) + C.x * (A.y - B.y));
	return center;
}

#define selfdot(myvec) dot(myvec, myvec)

VoronoiResult voronoi(float2 x, float2 driftScale, bool skew)
{
	int i, j;
	VoronoiResult res;

#if 1
	// slower, but better handles big numbers
	float2 worldBL = floor(x);
	float2 scanX = frac(x);
	float2 h = step(.5, scanX) - 2.;
	worldBL += h; scanX -= h;
#else
	float2 worldBL = floor(x - 1.5);
	float2 scanX = x - worldBL;
#endif

	//----------------------------------
	// first pass: regular voronoi
	//----------------------------------
	float2 bestClose;

	float mDist = 18.0;
	for (j = 0; j <= 3; j++)
		for (i = 0; i <= 3; i++)
		{
			float2 iter2 = float2(float(i), float(j));
			float2 cand = iter2cand(worldBL, scanX, iter2, driftScale, skew);
			float dist = dot(cand, cand);

			if (dist < mDist)
			{
				mDist = dist;
				bestClose = cand;
			}
		}

	//----------------------------------
	// second pass: distance to borders
	//----------------------------------
	mDist = 18.0;
	float2 bestFar;
	for (j = 0; j <= 3; j++)
		for (i = 0; i <= 3; i++)
		{
			float2 iter2 = float2(float(i), float(j));
			float2 cand = iter2cand(worldBL, scanX, iter2, driftScale, skew);

			float farDist = dot(0.5*(bestClose + cand), normalize(cand - bestClose));
			// skip the same cell
			if (dot(bestClose - cand, bestClose - cand) > EPS) {
				if (farDist < mDist) {
					mDist = farDist;
					bestFar = cand;
				}
			}
		}
	float mBorder = mDist;
	res.f1 = length(bestClose);
	res.f2 = length(bestFar);
	res.fBorder = sqrt(mBorder);
	res.world1 = bestClose + scanX + worldBL;
	res.world2 = bestFar + scanX + worldBL;
	res.norm = normalize(res.world1 - res.world2);

	return res;
}

#endif


/*
	res.t1 = res.t2 = res.t3 = 0;
	res.b1 = res.b2 = res.b3 = 0;
	
	//third pass
	float2 oldCCenter = bestClose;
	float  oldCRadius = 100.0; //needs to be like.. 4, but whatever
	for (j = -1; j <= 4; j++) {
		for (i = -1; i <= 4; i++) {
			float2 iter2 = float2(float(i), float(j));
			float2 candI = iter2cand(worldBL, scanX, iter2, driftScale, skew);
			for (int p = -1; p <= 4; p++) {
				for (int q = -1; q <= 4; q++) {
					float2 iterP = float2(float(p), float(q));
					float2 candP = iter2cand(worldBL, scanX, iterP, driftScale, skew);
					float2 B = candP, A = bestClose, C = candI;
					if (selfdot(A - B) >  EPS && selfdot(B - C) > EPS && selfdot(A - C) > EPS) {
						float3 bary = barycentric(float2(0, 0), A, B, C);
						bool inTriangle = baryInTriangle(bary);
						float2 candCenter = circumcenter(A, B, C);
						float candRadius = selfdot(candCenter - A);

						bool oldIncludesNew = selfdot(candP - oldCCenter) < (oldCRadius - EPS) ||
							selfdot(candI - oldCCenter) < (oldCRadius - EPS) ||
							selfdot(bestClose - oldCCenter) < (oldCRadius - EPS) ||
							selfdot(bestFar - oldCCenter) < (oldCRadius - EPS);

						if (inTriangle && oldIncludesNew)
						{
							oldCCenter = candCenter;
							oldCRadius = candRadius;
							res.b3 = bary.z;
							res.b1 = bary.x;
							res.b2 = bary.y;
							res.t1 = bestClose + scanX + worldBL;
							res.t2 = candP + scanX + worldBL;
							res.t3 = candI + scanX + worldBL;
						}
					}
				}
			}
		}
	}

	for (j = -1; j <= 4; j++) {
		for (i = -1; i <= 4; i++) {
			float2 iter2 = float2(float(i), float(j));
			float2 candI = iter2cand(worldBL, scanX, iter2, driftScale, skew);
			for (int p = -1; p <= 4; p++) {
				for (int q = -1; q <= 4; q++) {
					float2 iterP = float2(float(p), float(q));
					float2 candP = iter2cand(worldBL, scanX, iterP, driftScale, skew);
					float2 B = candP, A = bestFar, C = candI;
					if (selfdot(A - B) > EPS && selfdot(B - C) > EPS && selfdot(A - C) > EPS)
					{
						float3 bary = barycentric(float2(0, 0), A, B, C);
						bool inTriangle = baryInTriangle(bary);
						float2 candCenter = circumcenter(A, B, C);
						float candRadius = selfdot(candCenter - A);

						bool oldIncludesNew = selfdot(candP - oldCCenter) < (oldCRadius - EPS) ||
											  selfdot(candI - oldCCenter) < (oldCRadius - EPS) ||
											  selfdot(bestClose - oldCCenter) < (oldCRadius - EPS) ||
											  selfdot(bestFar - oldCCenter) < (oldCRadius - EPS);

						if (inTriangle && oldIncludesNew)
						{
							oldCCenter = candCenter;
							oldCRadius = candRadius;
							res.b3 = bary.z;
							res.b1 = bary.x;
							res.b2 = bary.y;
							res.t1 = bestFar + scanX + worldBL;
							res.t2 = candP + scanX + worldBL;
							res.t3 = candI + scanX + worldBL;
						}
					}
				}
			}
		}
	}

*/