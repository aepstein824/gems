﻿float3x3 inverse(float3x3 input)
{
#define minor(a,b) determinant(float2x2(input.a, input.b))

	float3x3 cofactors = float3x3(
		minor(_22_23, _32_33),
		minor(_13_12, _33_32),
		minor(_12_13, _22_23),

		minor(_23_21, _33_31),
		minor(_11_13, _31_33),
		minor(_13_11, _23_21),

		minor(_21_22, _31_32),
		minor(_12_11, _32_31),
		minor(_11_12, _21_22)
		);
#undef minor
	return cofactors / determinant(input);
}

#define M_PI 3.1415926535

#ifdef VERTEX_NOISE

#define BASE_NOISE_NAME baseNoiseV
#define NOISE_NAME noiseV
#define HALF_NAME halfSumNoiseV
#define EXP_NAME expSumNoiseV
#define CORNER_NAME cornerValueV
#define GEN_NAME noiseGenV
#define TURB_NAME turbulenceV
#define TAN_NORM_NAME tanNormalV

		static float3 g[16] = {
		   float3( 1.0f,  1.0f,  0.0f),     
		   float3(-1.0f,  1.0f,  0.0f),     
		   float3( 1.0f, -1.0f,  0.0f),    
		   float3(-1.0f, -1.0f,  0.0f),
		   float3( 1.0f,  0.0f,  1.0f),    
		   float3(-1.0f,  0.0f,  1.0f),     
		   float3( 1.0f,  0.0f, -1.0f),    
		   float3(-1.0f,  0.0f, -1.0f),
		   float3( 0.0f,  1.0f,  1.0f),    
		   float3( 0.0f, -1.0f,  1.0f),    
		   float3( 0.0f,  1.0f, -1.0f),    
		   float3( 0.0f, -1.0f, -1.0f),
		   float3( 1.0f,  1.0f,  0.0f),    
		   float3( 0.0f, -1.0f,  1.0f),    
		   float3(-1.0f,  1.0f,  0.0f),     
		   float3( 0.0f, -1.0f, -1.0f)
		};
		static int permutations[256] = {
			112 ,149 ,247, 5, 244, 66, 197, 1, 108, 167, 
			205, 231, 69, 20, 89, 47, 117, 119, 121, 65, 
			215, 36, 126, 41, 92, 129, 240, 162, 14, 225, 
			105, 73, 57, 74, 209, 165, 199, 216, 141, 37, 
			224, 109, 103, 204, 12, 210, 146, 80, 107, 40, 
			180, 17, 123, 131, 22, 2, 25, 42, 43, 79, 
			100, 207, 26, 175, 213, 59, 171, 137, 90, 39, 
			56, 150, 138, 201, 181, 219, 32, 110, 218, 67, 
			195, 95, 23, 94, 243, 223, 148, 226, 249, 18, 
			200, 10, 78, 8, 208, 155, 170, 164, 13, 31, 
			161, 62, 9, 250, 191, 98, 106, 97, 177, 182, 
			147, 227, 254, 217, 185, 0, 151, 239, 187, 114, 
			241, 179, 46, 152, 99, 133, 163, 53, 238, 30, 
			48, 142, 232, 120, 233, 222, 136, 33, 173, 198, 
			68, 122, 87, 77, 45, 72, 235, 255, 61, 186, 
			63, 28, 248, 52, 15, 158, 125, 237, 24, 183, 
			242, 96, 54, 93, 104, 190, 91, 88, 168, 156, 
			11, 58, 81, 253, 21, 27, 252, 102, 194, 116, 
			124, 246, 203, 206, 60, 134, 143, 172, 115, 189, 
			245, 85, 211, 55, 140, 118, 128, 221, 71, 144, 
			49, 3, 166, 153, 19, 34, 44, 16, 228, 111, 
			70, 234, 83, 160, 188, 75, 76, 29, 7, 82, 
			4, 132, 230, 50, 139, 113, 51, 178, 38, 86, 
			169, 6, 157, 196, 212, 154, 127, 159, 176, 193, 
			192, 214, 229, 236, 251, 130, 101, 35, 135, 184, 
			174, 145, 202, 220, 64, 84
		};

		struct GEN_NAME {
			float side;
		};

		int hash(float3 corner) {
			//return (  ((int(corner.x) & 7))
		    //        | ((int(corner.y) & 7) << 3)
		    //        | ((int(corner.z) & 3) << 6)) & 0xF;
		    return permutations[(  ((int(corner.x) & 7))
		                         + ((int(corner.y) & 7) << 3)
		                         + ((int(corner.z) & 3) << 6))] & 0xF;
		}

		float CORNER_NAME (GEN_NAME gen, float3 corner, float3 pos) {
			float lum = dot(g[hash(corner)], 
			                float3(pos - corner));
			return  lum;
		}

#else

#define BASE_NOISE_NAME baseNoise
#define NOISE_NAME noise
#define HALF_NAME halfSumNoise
#define EXP_NAME expSumNoise
#define CORNER_NAME cornerValue
#define GEN_NAME noiseGen
#define TURB_NAME turbulence
#define GRAD_NAME gradient
#define TAN_NORM_NAME tanNormal

		struct noiseGen {
			sampler3D tex;
			float side;
		};

		float CORNER_NAME (GEN_NAME gen, float3 corner, float3 pos) {
			float lum = dot(tex3D (gen.tex, corner / gen.side).xyz * 2 - 1, 
			                float3(pos - corner));
			return  lum;
		}
#endif
	
		float BASE_NOISE_NAME (GEN_NAME gen, float3 pos) {
			// frac works because of int resolution

		    float3 texPos = frac (pos);
		    float3 intPos = texPos * gen.side;
		    float3 corner000 = floor(intPos);
		    float3 corner111 = corner000 + float3(1,1,1);
		    float3 X = float3(1,0,0);
		    float3 Y = float3(0,1,0);
		    float3 Z = float3(0,0,1);

		    float3 interps = float3(smoothstep (corner000, corner111, intPos));

		    float xy00 = lerp (CORNER_NAME(gen, corner000, intPos),
		                       CORNER_NAME(gen, corner000 +Z, intPos),
		                       interps.z);
		    float xy10 = lerp (CORNER_NAME(gen, corner000 +X, intPos),
		                       CORNER_NAME(gen, corner000 +X+Z, intPos),
		                       interps.z);
		    float xy01 = lerp (CORNER_NAME(gen, corner000 +Y, intPos),
		                       CORNER_NAME(gen, corner000 +Y+Z, intPos),
		                       interps.z);
		    float xy11 = lerp (CORNER_NAME(gen, corner000 +X+Y, intPos),
		                       CORNER_NAME(gen, corner000 +X+Y+Z, intPos),
		                       interps.z);	

		    float x0 = lerp (xy00, xy01, interps.y);
		    float x1 = lerp (xy10, xy11, interps.y);
   		    float middle = lerp (x0, x1, interps.x);

   		    return middle;
		}
		
		// this noise varies based on complex numbers
		float NOISE_NAME(GEN_NAME gen, float3 pos) {
			float3 iPos = pos + .5 * float3(0.5f, 0.5f, 0.5f);
			float3 lPos = pos / 3.0f;
			float hR = BASE_NOISE_NAME(gen, pos);
			float hI = BASE_NOISE_NAME(gen, iPos);
			float l = BASE_NOISE_NAME(gen, lPos);
			return hR * cos(2 * M_PI * l) + hI * sin(2 * M_PI * l);
		}

		float EXP_NAME (GEN_NAME gen, float3 pos, float rat, float iters, float cutoff) {
			float noiseSum = 0;
		   	for (float i = 0; i < iters; i++) {
				float cutoffWeight = saturate(cutoff - i); //0 when i = cutoff
		      	noiseSum += cutoffWeight / pow(rat, i) 
					* NOISE_NAME(gen, pos * exp2(i));
		   	}
		   	return noiseSum;
		}

		float EXP_NAME(GEN_NAME gen, float3 pos, float rat, float iters) {
			return EXP_NAME (gen, pos, rat, iters, iters);
		}

		float HALF_NAME (GEN_NAME gen, float3 pos, int iters) {
			return EXP_NAME (gen, pos, 2.0f, iters);
		}

		// TURBULENCE TEXTURE
		float TURB_NAME (GEN_NAME gen, float3 pos) {
			float t = -.5 + HALF_NAME (gen, pos, 3);
			return t;
		}

#define EPS (.0001f)
#define GRAD3D (pos, noiseSum, noiseFunc) ((float3(noiseFunc(pos + float3(EPS, 0, 0)),\
												   noiseFunc(pos + float3(0, EPS, 0)),\
												   noiseFunc(pos + float3(0, 0, EPS)))\
											- noiseSum * float3(1, 1, 1)) / EPS)


		float3 TAN_NORM_NAME(float3 tangent, float3 normal, float3 dGrad) {
			float3 binormal = normalize(cross(normal, tangent));
			float3x3 tangentToWorld = transpose(float3x3(tangent, binormal, normal));
			float3x3 worldToTangent = inverse(tangentToWorld);
			float3 tanDGrad = mul(worldToTangent, dGrad);
			return normalize(float3(0, 0, 1) - EPS * tanDGrad);
		}

#undef NOISE_NAME
#undef HALF_NAME
#undef EXP_NAME
#undef CORNER_NAME
#undef GEN_NAME
#undef TURB_NAME
#undef TAN_NORM_NAME

