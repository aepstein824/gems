﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/fire"
{
	Properties
	{
		_MainTex ("Perlin Noise", 3D) = "cube" {}
		_Size ("Size", Range(0, 10)) = 1
		_Side ("Side", int) = 1
		_Base ("Base", Vector) =  (0, 0, 0)
		_Height ("Height", float) = 1
		_kH ("k", float) = 1.4
		_t ("Time", float) = 0

		_Flicker ("Flicker", Vector) = (0, 1, 0)
		_MainColor ("Main Color", Color) = (1, 1, 1, 1)
		_FadeColor ("Fade Color", Color) = (1, 1, 0, 1)
		_FringeColor ("Frine Color", Color) = (1, 0, 0, 1)
	}
	SubShader
	{

		Tags { "Queue" = "Transparent" "RenderType" = "Transparent"} 
	    ZWrite Off // don't write to depth buffer 
            // in order not to occlude other objects

        Blend SrcAlpha OneMinusSrcAlpha // use alpha blending

		Pass
		{
			CGPROGRAM
			#pragma target 3.0

			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include  "../perlinnoise.cginc"
			#define VERTEX_NOISE
			#include  "../perlinnoise.cginc"
			#undef VERTEX_NOISE

			sampler3D _MainTex;
			float _Size;
			int _Side;
			float3 _Base;
			float _Height;
			float _kH;
			float _t;
			float3 _Flicker;
			fixed4 _FringeColor, _FadeColor, _MainColor;	

			struct appdata
			{
				float4 vertex : POSITION;
				float4 normal : NORMAL;
			};

			struct v2f
			{
				float3 pos : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;

				v.vertex.xyz /= v.vertex.w;
				v.vertex.w = 1.0;
				v.vertex.xz /= v.vertex.y > 0 ? v.vertex.y + 1 : 1;

				o.pos = mul (unity_ObjectToWorld, v.vertex);
				o.vertex = float4 (o.pos, 1);
				o.vertex = mul (UNITY_MATRIX_VP, o.vertex);

				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				//return fixed4 (1, 0, 0, 1);

				float3 pos = i.pos / _Size;

				noiseGen gen = { _MainTex, _Side };
				fixed noiseSum = halfSumNoise (gen, pos - float3(0,_t,0), 2);

		    	float relH = (pos.y - _Base.y) / _Height; 

		    	fixed noiseVal = pow (noiseSum * .5 + .5, pow(relH, _kH));//;


				// sample the texture
				fixed4 col = noiseVal * _FringeColor;
				col += pow(noiseVal, 3) * _FadeColor;
				col += pow(noiseVal, 4) * _MainColor;
				return col;
			}
			ENDCG
		}
	}
}
