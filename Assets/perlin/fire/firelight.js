﻿#pragma strict

public var frequency:float = 4;
public var heightScale:float = .3;
public var lightScale:float = .2;
public var windScale:float = .1;

private var startPos:Vector3;
private var lt:Light;

public var _MainColor:Color;
public var _FadeColor:Color;
public var _FringeColor:Color;

function Start () {
    transform.parent.transform.GetChild(0).GetComponent.<Renderer>().material.SetColor("_MainColor", 
	                                            _MainColor);
	transform.parent.transform.GetChild(0).GetComponent.<Renderer>().material.SetColor("_FadeColor", 
	                                            _FadeColor);
	transform.parent.transform.GetChild(0).GetComponent.<Renderer>().material.SetColor("_FringeColor", 
	                                            _FringeColor);

	GetComponent.<Light>().color= _FadeColor;
    startPos = transform.position;
    lt = GetComponent.<Light>();
}

function Update () {
    //var x:float = Mathf.Sin (frequency * Time.realtimeSinceStartup) * .5 + .5;
    var flicker:float = Mathf.PerlinNoise (frequency * Time.time, .5);

    transform.position.y = startPos.y + flicker * heightScale;
    lt.intensity = (1 - (1 - flicker) * lightScale) * transform.lossyScale.y;

    var wind:Vector2 = Vector2 (Mathf.PerlinNoise (frequency * Time.time, 1.5) - .5,
                                Mathf.PerlinNoise (frequency * Time.time, 2.5) - .5);
    wind *= windScale * 2;
    transform.position.x = startPos.x + wind.x;
    transform.position.z = startPos.z + wind.y;

	transform.parent.transform.GetChild(0).GetComponent.<Renderer>().material.SetVector ("_Flicker",
	                                              Vector3 (wind.x, flicker * heightScale, wind.y));
	transform.parent.transform.GetChild(0).GetComponent.<Renderer>().material.SetVector ("_Base",
												  transform.parent.transform.position);
	transform.parent.transform.GetChild(0).GetComponent.<Renderer>().material.SetFloat ("_Height",
												  transform.lossyScale.y);
}