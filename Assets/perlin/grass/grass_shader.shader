﻿Shader "Custom/grass" {
	Properties{
		_PerlinVerts("Perlin Noise", 3D) = "cube" {}
		_Side("Side", int) = 1

		_kSpot("k Spot", float) = 0
		_iters("num iterations", int) = 3
		_kRat("iteration ratio", Range(1.0, 3)) = 2
		_kScale("position scale", Vector) = (1, 1, 1)
		_lowF ("lowF", Vector) = (0.15, 1.25, 40, 1)
		_kWhite("white", Color) = (1, 1, 1, 1)
		_kBlack("black", Color) = (0, 0, 0, 1)

		_PlayerPos("player position PROCEDURAL", Vector) = (0, 0, 0, 1)
		_FOV("field of view PROCEDURAL", float) = 60
		_Resolution("resolution PROCEDURAL", float) = 1080
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf Standard fullforwardshadows vertex:vert

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			#include  "../perlinnoise.cginc"
			#include  "../hashnoise.cginc"  

			sampler3D _PerlinVerts;


			struct Input {
				float3 worldPos;
				float3 sWorldNormal;
				float4 sWorldTangent;
			};

			int _Side;
			float _kSpot;
			int _iters;
			float _kRat;
			float3 _kScale;
			float4 _kWhite, _kBlack;
			float4 _PlayerPos;
			float _Resolution, _FOV;
			float4 _lowF;

			void vert(inout appdata_full v, out Input o)
			{
				UNITY_INITIALIZE_OUTPUT(Input, o);
				//v.normal = (float4( v.normal.x, v.normal.y, v.normal.z, 1.0)).xyz;

				o.sWorldNormal = mul((float3x3)unity_ObjectToWorld, SCALED_NORMAL);
				o.sWorldTangent = mul(v.tangent, unity_ObjectToWorld);
			}

			float bwnoise(float x) {
				return x < 0 ? 0 : 1;
			}



			/*name	var	units
			basic noise	N	1 freq/meter
			scale	K	scalar, divides N
			resolution	W	pix / screen
			dist + fov	C	meters / screen, distance * tan (fov)
			fudge	X	scalar, times N
			nyquist	p/s > f/s
			noise = expSum (X*pos/K)
			X < WK/C
			*/
			float maxDetailIters(float3 pos) {
				float cameraDist = distance(pos, _PlayerPos.xyz);
				float metersPerScreen = cameraDist * tan(M_PI / 180 * _FOV);
				float maxFudge = _Resolution * _kScale.y / metersPerScreen;
				float numFudge = log2(maxFudge) - 1;
				return numFudge;
			}

			float to1(float x) { return x * 0.5f + 0.5f; }
			float to2(float x) { return x * 2 - 1; }

			float rockNoise(float3 pos) {
				float numFudge = maxDetailIters(pos);
				pos /= _kScale;

				noiseGen gen = { _PerlinVerts, _Side };
				float noiseVal = .5f * expSumNoise(gen, pos, _kRat, (float)_iters, numFudge) + _kSpot + 0.5f;
				return noiseVal;
			}

			void surf(Input IN, inout SurfaceOutputStandard o) {
				noiseGen gen = { _PerlinVerts, _Side };
				
				float3 pos = IN.worldPos;

				float3 tangent = normalize(IN.sWorldTangent.xyz);
				float3 normal = normalize(IN.sWorldNormal);
				float  noiseSum = rockNoise(pos);
				float3 dGrad = ((float3(rockNoise(pos + float3(EPS, 0, 0)),
										rockNoise(pos + float3(0, EPS, 0)),
										rockNoise(pos + float3(0, 0, EPS)))
								  - noiseSum * float3(1, 1, 1)) / EPS);
				float3 tanNorm = tanNormal(tangent, normal, dGrad);

				float interp = noiseSum;
	
				 
				float3 lowFreqColor = float3(expSumNoise(gen, pos / _lowF.z, _lowF.y, 6),
					expSumNoise(gen, (pos + float3(0,1,0)) / _lowF.z, _lowF.y, 3),
					expSumNoise(gen, (pos + float3(0,0,1)) / _lowF.z, _lowF.y, 3));
				_kBlack.xyz *= 1 + _lowF.x * lowFreqColor.x;
				//_kWhite.xyz = lerp(_kWhite, _kWhite.yzx, .3 * cellDir);

				float4 noiseColor = lerp(_kBlack, _kWhite, interp);

				fixed4 c = noiseColor;
				o.Albedo = c.rgb;
				o.Smoothness = 0;// noiseSum * 0.1f;
				o.Alpha = c.a;
				o.Normal = tanNorm;
				o.Metallic = 0;
			}
			ENDCG
		}
			FallBack "Diffuse"
}
