﻿Shader "Custom/rock" {
	Properties{
		_MainTex("Perlin Noise", 3D) = "cube" {}
		_Side("Side", int) = 1

		_BaseColor("Base Color", Color) = (0.5, 0.3, 0.1, 1.0)
		_GrainColor("Grain Color", Color) = (0.45, 0.25, 0.2, 1.0)
		_HighlightColor("Highlight Color", Color) = (1.0, 0, 0, 1.0)
		_CrackColor("Crack Color", Color) = (0, 0, 0, 1)
		_WearColor("Wear Color", Color) = (1, 1, 1, 1)

		_Grain("RockGrain (k, iters, scale, decay)", Vector) = (0.5, 2, 1, 1.1)

		_WindCracks("WindCracks(k)", float) = 1.0
		_WearEnvironment("WearEnvironment(k, iters, scale, decay)", Vector) = (0.5, 2, 1, 1.1)

		_surfWeightsX("surface matrix X, N", Vector) = (1,0,0,0)
		_surfWeightsY("surface matrix Y, N", Vector) = (0,0,1,0)
		_vorScale("voronoi scale (crack / invis)", Vector) = (1, 1, 1, 1)
		_crack("crack (blur, numIters, scale, thresh)", Vector) = (-.35, 1.25, 40, -.34)
		_cell("cell(crack.dw/decay/invis.w)", Vector) = (1, 1, 1, 1)
		_crackShake("CrackShake(kShake, 0, shake, invisBias)", Vector) = (1, 1, 1, 1)

		_PlayerPos("PROCEDURAL player position", Vector) = (0, 0, 0, 1)
		_FOV("PROCEDURAL field of view", float) = 60
		_Resolution("PROCEDURAL resolution", float) = 1080
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf Standard fullforwardshadows vertex:vert

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			#include  "../perlinnoise.cginc"
			#include  "../hashnoise.cginc"  
			#include "../voronoi.cginc"



			struct Input {
				float3 worldPos;
				float3 sWorldNormal;
				float4 sWorldTangent;
			};

			sampler3D _MainTex;
			int _Side;
			float4 _BaseColor, _GrainColor, _Highlight, _CrackColor, _WearColor;
			float4 _Grain;
			float _WindCracks;
			float4 _WearEnvironment;
			float4 _surfWeightsX;
			float4 _surfWeightsY;
			float4 _vorScale;
			float4 _crack;
			float4 _cell;
			float4 _crackShake;

			float4 _PlayerPos;
			float _Resolution, _FOV;

			void vert(inout appdata_full v, out Input o)
			{
				UNITY_INITIALIZE_OUTPUT(Input, o);
				//v.normal = (float4( v.normal.x, v.normal.y, v.normal.z, 1.0)).xyz;

				o.sWorldNormal = mul((float3x3)unity_ObjectToWorld, SCALED_NORMAL);
				o.sWorldTangent = mul(v.tangent, unity_ObjectToWorld);
			}

			float bwnoise(float x) {
				return x < 0 ? 0 : 1;
			}

			float steps(float x, float perUnit) {
				float freq = 2 * M_PI * perUnit;
				return x + sin(freq * x) / freq;
			}

			float2 toSurface(float3 worldPos) {
				return float2(dot(worldPos, _surfWeightsX.xyz), 
					dot(worldPos, _surfWeightsY.xyz));
			}

			float3 toPerlin(float2 surface) {
				return float3(surface.xy, 0.5f);
			}

			/*name	var	units
			basic noise	N	1 freq/meter
			scale	K	scalar, divides N
			resolution	W	pix / screen
			dist + fov	C	meters / screen, distance * tan (fov)
			fudge	X	scalar, times N
			nyquist	p/s > f/s
			noise = expSum (X*pos/K)
			X < WK/C
			*/
			float maxDetailIters(float3 pos, float3 scale) {
				float cameraDist = distance(pos, _PlayerPos.xyz);
				float metersPerScreen = cameraDist * tan(M_PI / 180 * _FOV);
				float maxFudge = _Resolution * scale.y / metersPerScreen;
				float numFudge = log2(maxFudge) - 1;
				return numFudge;
			}

			float3 desaturateColor(float3 satCol, float amount) {
				float greyVal = dot(satCol, float3(.3, .6, .1));
				return lerp(satCol, greyVal * float3(1, 1, 1), amount);
			}

			float to1(float x) { return x * 0.5f + 0.5f; }
			float to2(float x) { return x * 2 - 1; }
			float from0(float x) { return 1 - 2 * abs(x);  }

			float rockPerlin(float3 worldPos, float4 parameters) {
				float2 pos = toSurface(worldPos) / parameters.z;

				noiseGen gen = { _MainTex, _Side };
				float noiseVal = parameters.x * expSumNoise(gen, toPerlin(pos),
					parameters.w, parameters.y);
				return noiseVal;
			}

			float lowfWear(float3 worldPos) {
				return rockPerlin(worldPos, _WearEnvironment);
			}

			float grainNoise(float3 worldPos) {
				return rockPerlin(worldPos, _Grain);
			}

			struct CrackResult {
				float isCrack;
				float fromCrack;
			};

			CrackResult crackSum(float3 worldPos) {
				float2 pos = toSurface(worldPos);
				
				float vorExpTotal = 0;
				float definite = 0.0f;
				float maybe = 0.0f;
				CrackResult crackRes;

				//first iteration is invisible and requires different scale
				VoronoiResult invisCracks = voronoi(pos / _vorScale.zw, 
					float2(_surfWeightsX.w,_surfWeightsY.w), false);
				float invisCellBias = _crackShake.w * to2(hash12(invisCracks.world1));
				maybe = saturate(1 - smoothstep(0, _cell.w + invisCellBias, invisCracks.fBorder));

				for (float i = 0; i < _crack.y; i++) {
					float2 iPos = pos * pow(_cell.z, i);// +0.5f * (i + 1) * sin((i + 1) * pos);

					noiseGen gen = { _MainTex, _Side };
					float crackShake = _crackShake.x * noise(gen, float3(iPos / _crackShake.z, 0.5f));

					VoronoiResult vorResults = voronoi(iPos/ _vorScale.xy, 
						float2(_surfWeightsX.w,_surfWeightsY.w), false);
					float distanceFromBorder = vorResults.fBorder;// / pow(_vorScale.w, i);
					distanceFromBorder = abs(distanceFromBorder - crackShake * sign(vorResults.norm.x));
					
					definite = max(definite, smoothstep(0 , distanceFromBorder, pow(maybe, 1/(1 + i)) * _cell.x));
					maybe *= saturate(1 - smoothstep(0, _cell.y, distanceFromBorder));
					crackRes.fromCrack += sqrt(saturate(maybe)) / _crack.y;
				}

				crackRes.isCrack = definite;

				return crackRes;
			}


			void surf(Input IN, inout SurfaceOutputStandard o) {
				noiseGen gen = { _MainTex, _Side };
				
				float3 pos = IN.worldPos;

				//float3 tangent = normalize(IN.sWorldTangent.xyz);
				//float3 normal = normalize(IN.sWorldNormal);
				/*float  noiseSum = rockNoise(pos);
				float3 dGrad = ((float3(rockNoise(pos + float3(EPS, 0, 0)),
										rockNoise(pos + float3(0, EPS, 0)),
										rockNoise(pos + float3(0, 0, EPS)))
								  - noiseSum * float3(1, 1, 1)) / EPS);*/
				//float3 dGrad = float3(1, 1, 1);
				//float3 tanNorm = tanNormal(tangent, normal, dGrad);

				CrackResult crackRes = crackSum(pos);

				float wear = _WindCracks * crackRes.fromCrack + saturate(lowfWear(pos));

				float4 noiseColor;
				noiseColor = _BaseColor;
				noiseColor = lerp(noiseColor, _GrainColor, grainNoise(pos));
				noiseColor = lerp(noiseColor, _WearColor, wear);
				noiseColor = lerp(noiseColor, _CrackColor, crackRes.isCrack);
				fixed4 c = noiseColor;
				o.Albedo = c.rgb;
				o.Smoothness = 0.1f;
				o.Alpha = c.a;
				//o.Normal = tanNorm;
			}
			ENDCG
		}
			FallBack "Diffuse"
}
