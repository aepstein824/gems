﻿#pragma strict

public var lightDisable:GameObject[];
public var lightEnable:GameObject[];
public var renderEnable:GameObject[];
public var renderDisable:GameObject[];

public var newAmbient:Color;

function Start () {

}

function Update () {

}

function switchOn (on:boolean) {
	var lights:Array = new Array();
	for (var g:GameObject in lightDisable) {
		getLights(lights, g, Light);
	}	
	for (var l:Light in lights) {
		l.enabled = !on;
	}

	lights = new Array();
	for (var g:GameObject in lightEnable) {
		getLights(lights, g, Light);
	}	
	for (var l:Light in lights) {
		l.enabled = on;
	}

	lights = new Array();
	for (var g:GameObject in renderDisable) {
		getLights(lights, g, Renderer);
	}	
	for (var l:Renderer in lights) {
		l.enabled = !on;
	}

	lights = new Array();
	for (var g:GameObject in renderEnable) {
		getLights(lights, g, Renderer);
	}	
	for (var l:Renderer in lights) {
		l.enabled = on;
	}
}

function OnTriggerEnter (other:Collider) {
	RenderSettings.ambientLight = newAmbient;

	switchOn(true);
}

function OnTriggerExit (other:Collider) {
	switchOn(false);
}


function getLights (lights:Array, parent:GameObject, type:System.Type) {
	var l:Component = parent.GetComponent(type);
	if (l != null) {
		lights.Add(l);
	}
	for (var child:Transform in parent.transform) {
		getLights(lights, child.gameObject, type);
	}
}