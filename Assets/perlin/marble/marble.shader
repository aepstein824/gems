﻿Shader "Custom/marble" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Perlin Noise", 3D) = "cube" {}
		_Side ("Side", int) = 1
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_kSpot ("k Spot", float) = 0
		_iters ("num iterations", int) = 3
		_kRat ("iteration ratio", Range(1.1, 3)) = 2
		_kThick ("marble thickness", float) = .5
		_kScale ("position scale", Vector) = (1,1,1)

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		#include  "../perlinnoise.cginc"


		sampler3D _MainTex;

		struct Input {
			float3 worldPos;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		int _Side;
		float _kSpot;
		int _iters;
		float _kRat;
		float _kThick;
		float3 _kScale;

		float marbNoise (float x) {
			return pow (abs(x), _kThick);
		}

		float bwNoise (float x) {
			return x < .5 ? 0 : 1;
		}

		float sharpen (float x) {
			return pow(abs(x), .5) * sign(x);
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			float3 pos = IN.worldPos;
			pos /= _kScale;

			noiseGen gen = { _MainTex, _Side };
			float3 stretch = pos;
			stretch.y /= 3;
			stretch.x += .3 * pos.y;
			stretch.z += .3* pos.y;
			float noiseSum = marbNoise(expSumNoise (gen, stretch, _kRat, _iters) + _kSpot); 

			float lowFreq = sharpen (halfSumNoise (gen, stretch / 4, 2)) *.5 + .5;

			fixed4 c = (1 - .2 * (1-noiseSum) * lowFreq) * _Color;
			//c = lowFreq * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
