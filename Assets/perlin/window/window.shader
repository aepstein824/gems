﻿Shader "Custom/window" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)

		_PlayerPos ("Player Transform Pos", Vector) = (0, 1, 0)
		_MainTex ("Albedo (RGB)", 3D) = "white" {}
		_WindowTex ("Looker Render Texture", 2D) = "" {}
		_kernelW ("width of kernel", float) = 0.0039
		_kernelMax ("kernel Samples", int) = 1
		_kernelDisp ("kernel displacement", Range(0,1)) = .1
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		#pragma surface surf NoLighting

		#include "../perlinnoise.cginc"


		struct Input {
			float2 uv_MainTex;
		};

		sampler3D _MainTex;
		sampler2D _WindowTex;

		half   _Glossiness;
		half   _Metallic;
		fixed4 _Color;
		float  _kernelW;
		float  _kernelMax;
		float  _kernelDisp;
		float4 _PlayerPos;

		fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
     	{
        	fixed4 c;
        	float3 norm = s.Normal * -1; //assume window is facing -z
        	float3 toWindow = _PlayerPos.xyz - float3(2.5, 1.8, -7);
        	float through = -1 * dot(normalize(toWindow), norm);
        	c.rgb = lerp (_Color.xyz*.3, s.Albedo, through);
        	//c.rgb = float3 ( through,through,through);
        	//c.rgb = norm;
        	//c.rgb = float3 (0, 0, 0);
        	//c.rgb = s.Albedo;
        	c.a = 1.0f;
         	return c;
     	}

		void surf (Input IN, inout SurfaceOutput o) {
			noiseGen gen  = {_MainTex, 16};

			float2 uv = IN.uv_MainTex;
			float2 disp = _kernelDisp * float2 (noise(gen, float3(uv*4, 0)),
			                                    noise(gen, float3(uv*4, .5)));
			uv += disp;
			o.Normal = normalize (float3(0, 0, 1) - float3(disp/_kernelDisp*.6, 0));

			fixed4 c = fixed4 (0, 0, 0, 0);
			for (int i = -_kernelMax; i <= _kernelMax; i++)
				for (int j = -_kernelMax; j <= _kernelMax; j++)
				    c += tex2D (_WindowTex, uv + _kernelW * float2(j, i));		
			c *= _Color / pow(2 * _kernelMax + 1, 2);

			o.Albedo = c.rgb;
		}



		ENDCG
	}
	FallBack "Diffuse"
}
