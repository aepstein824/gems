﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

                Shader "Unlit/window"
{
	Properties
	{
		_PlayerPos ("Player Transform Pos", Vector) = (0, 1, 0)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			float3 _PlayerPos;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 modelPos: TEXCOORD0;
			};

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.modelPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float3 dirToP = normalize(i.modelPos - _PlayerPos);
				float4 val = UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, dirToP);
				fixed4 refLight = 1;
				refLight.xyz = DecodeHDR(val, unity_SpecCube0_HDR);

				return refLight;
			}
			ENDCG
		}
	}
}
