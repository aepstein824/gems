﻿#pragma strict

public var looker:Camera;
public var player:GameObject;


function Start () {

}

function Update () {
	GetComponent.<Renderer>().material.SetVector ("_PlayerPos",
	                                              player.transform.position);
	looker.transform.position = player.transform.position;
	//var yAngle:float = Mathf.Rad2Deg* Mathf.Atan2(toWindow.x, toWindow.z);

	var toWindow:Vector3 = transform.position - looker.transform.position;
	var toRight:Vector3 = toWindow + .5 * Vector3.right;
	var toLeft:Vector3 = toWindow - .5 * Vector3.right;
	var toUp:Vector3 = toWindow + .5 * Vector3.up;
	var fullAngle:float = Vector3.Angle(toRight, toLeft);
	var rightProj:float = Mathf.Tan(Mathf.Deg2Rad*Vector3.Angle(toWindow, toLeft));
	var upProj:float = Mathf.Tan(Mathf.Deg2Rad*Vector3.Angle(toWindow, toUp));
	looker.aspect = rightProj / upProj;
	looker.fieldOfView = fullAngle / looker.aspect;

	var angR:float = Mathf.Rad2Deg* Mathf.Atan2(toRight.x, toRight.z);
	var halfAngle:float = angR - .5 * fullAngle; 
	looker.transform.rotation = Quaternion.AngleAxis(halfAngle, Vector3.up);
}