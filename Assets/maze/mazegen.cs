﻿using UnityEngine;
using System.Collections.Generic;

public class mazegen : MonoBehaviour
{
    public Vector3[] verts;
    public int[] tris;
    public Vector3[] norms;
    public float perlinScale = 2;
    public int w = 1;
    public int h = 1;
    public int perSquare = 1;

    public GameObject setToNoise;
    public Texture2D noiseTex;

    float noiseFunc(float x, float y)
    {
        Vector3 modelSpace = new Vector3(x, 0, y);
        Vector3 transSpace = transform.TransformPoint(modelSpace);
        transSpace /= perlinScale;
        return (Mathf.Sin(6 * transSpace.x / 2) / 4 + Mathf.Sin(6 * transSpace.z / 2) / 4 + .95f) 
            * Mathf.PerlinNoise(transSpace.x, transSpace.z);
    }

    // Use this for initialization
    void Start()
    {
        //triangles
        Vector3[] midpoints = new Vector3[] { new Vector3(0.5f, 0, 1.0f),
                                              new Vector3(1.0f, 0, 0.5f),
                                              new Vector3(0.5f, 0, 0.0f),
                                              new Vector3(0.0f, 0, 0.5f) };
        int[,] triangles = new int[12, 2] { { 0, 1 }, { 0, 2 }, { 0, 3 },
                                            { 1, 0 }, { 1, 2 }, { 1, 3 },
                                            { 2, 0 }, { 2, 1 }, { 2, 3 },
                                            { 3, 0 }, { 3, 1 }, { 3, 2 } };
        int[,] squares = new int [16, 2] { { -1, -1 }, {  9, -1 }, {  0, -1 },
                                           { 10, -1 }, {  8, -1 }, {  6, -1 },
                                           {  4,  8 }, {  7, -1 }, {  4, -1 },
                                           {  9,  4 }, {  1, -1 }, { 11, -1 },
                                           {  5, -1 }, {  3, -1 }, {  2, -1 },
                                           { -1, -1 } };

        List<Vector3> uniqVerts = new List<Vector3>();
        List<int> triList = new List<int>();
        List<Vector3> vertNorms = new List<Vector3>();

        Color[] noiseColors = new Color[1024 * 1024];

        float size = 1.0f / perSquare;
        for (int i = 0; i < w * perSquare; i++)
        {
            for (int j = 0; j < h * perSquare; j++)
            {
                Vector3 topLeft = new Vector3(i * size, 0, j * size);
                for (int a = 0; a < 10; a++)
                {
                    for (int b = 0; b < 10; b++)
                    {
                        float noiseVal = noiseFunc(topLeft.x + .1f * a * size, topLeft.z + .1f * b * size);
                        Color noiseColor = new Color(noiseVal, noiseVal, noiseVal);
                        noiseColors[(5 * i + a) * 1024 + 5 * j + b] = noiseColor;
                    }
                }

                int square = ((noiseFunc(topLeft.x, topLeft.z + size) > 0.5f ? 1 : 0)
                    + (noiseFunc(topLeft.x + size, topLeft.z + size) > 0.5f ? 2 : 0)
                    + (noiseFunc(topLeft.x, topLeft.z) > 0.5f ? 4 : 0)
                    + (noiseFunc(topLeft.x + size, topLeft.z) > 0.5f ? 8 : 0));
                for (int k = 0; k < 2; k++)
                {
                    int tri = squares[square, k];
                    if (tri < 0)
                    {
                        continue;
                    }

                    int ind = uniqVerts.Count;

                    uniqVerts.Add(topLeft + size * midpoints[triangles[tri, 0]]);
                    uniqVerts.Add(topLeft + size * midpoints[triangles[tri, 0]]
                        + new Vector3(0, 1.0f, 0));
                    uniqVerts.Add(topLeft + size * midpoints[triangles[tri, 1]]);
                    uniqVerts.Add(topLeft + size * midpoints[triangles[tri, 1]]
                        + new Vector3(0, 1.0f, 0));
                    
                    triList.Add(ind);
                    triList.Add(ind + 1);
                    triList.Add(ind + 2);
                    triList.Add(ind + 3);
                    triList.Add(ind + 2);
                    triList.Add(ind + 1);

                    Vector3 triTan = uniqVerts[ind + 2] - uniqVerts[ind];
                    Vector3 triBin = uniqVerts[ind + 1] - uniqVerts[ind];
                    Vector3 triNorm = -1 * Vector3.Cross(triTan, triBin);

                    vertNorms.Add(triNorm);
                    vertNorms.Add(triNorm);
                    vertNorms.Add(triNorm);
                    vertNorms.Add(triNorm);
                }
            }
        }

        verts = uniqVerts.ToArray();
        tris = triList.ToArray();
        norms = vertNorms.ToArray();

        Mesh mesh = GetComponent<MeshFilter>().mesh;
        mesh.vertices = verts;
        mesh.triangles = tris;
        mesh.normals = norms;

        noiseTex = new Texture2D(1024, 1024, TextureFormat.RGBA32, true);
        noiseTex.SetPixels(noiseColors);
        noiseTex.filterMode = FilterMode.Point;
        noiseTex.Apply();
        setToNoise.GetComponent<Renderer>().material.SetTexture("_MainTex",
                                               noiseTex);
    }
    
    // Update is called once per frame
    void Update()
    {

    }
}
